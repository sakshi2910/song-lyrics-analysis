import os
from nltk.stem import PorterStemmer, WordNetLemmatizer
from nltk.corpus import stopwords
from nltk.tokenize import sent_tokenize, word_tokenize
import string
from collections import defaultdict,OrderedDict

#change path,g,filename[x:] for different lyricits

artists=["JoniMitchell","MichaelJackson","Nirvana","RollingStones","JanisJoplin","Madonna","Metallica","PattiSmith","Queen","BobDylan","BebeRexha","JenniferLopez","BritneySpears","ArianaGrande","BrunoMars","SelenaGomez","ShawnMendes","JustinBieber","TaylorSwift"]
path1='./'
phonemes={}
for a in artists:
	path=path1+a+'/lyrics'
	path2=path1+a+'/data/phonemes.txt'
	g=open(path2,'r')
	l=g.readlines()

	for i in l:
		x=i.strip().split()
		phonemes[x[0]]=' '.join(x[1:])
	print phonemes
	for fn in os.listdir(path):
		filename = os.path.join(path, fn)
		n=len(a)+10
		f=open(filename+'/'+filename[n:]+'.txt','r')
		text = f.read()
		f.close()
		text = text.translate(None, ',')
		text = text.translate(None, '"')
		text = text.translate(None, '.')
		text = text.translate(None, '...')
		text = text.translate(None, '?')
		text = text.translate(None, '!')
		text = text.translate(None, ';')
		text = text.translate(None, '-')
		text = text.translate(None, '\'')
		text = text.translate(None, '.\'')
		text = text.translate(None, '(')
		text = text.translate(None, ')')
		text = text.translate(None, ':')
		text = text.translate(None, '>')
		text = text.translate(None, '<')
		tokens = word_tokenize(text)
		tokens = [w.lower() for w in tokens]
		words = [word for word in tokens if word.isalpha()]
		
		sequence=[phonemes[i] for i in words if i in phonemes]
		dir1="./deepdataset/"+a
		if not os.path.exists(dir1):
			os.makedirs(dir1)
		f=open("./deepdataset/"+a+"/"+filename[n:]+'.txt','w')
		sequence=' '.join(sequence)
		f.write(sequence)
		f.close()
# g2p-seq2seq --decode /home/sakshi/BTPresult/TaylorSwift/data/unique_words.txt --model g2p-seq2seq-cmudict >> /home/sakshi/BTPresult/TaylorSwift//data/phonemes.txt
import os

artists=["BebeRexha","JenniferLopez","BritneySpears","JoniMitchell"]
path1 = "./"
for a in artists:

	path = path1+a+'/lyrics'
	path2=path1+a+'/data/phonemes.txt'
	phoneme_dict={}
	f=open(path2)
	lines=f.readlines()
	for l in lines:
		i=l.strip().split()
		phoneme_dict[i[0]]=[]
		phoneme_dict[i[0]].extend(i[1:])



	final={}

	for fn in os.listdir(path):
		phoneme_dist={}
		dirname = os.path.join(path, fn)
		file = open(dirname+'/'+'word_frequency.txt', 'rt')
		text = file.readlines()
		file.close()
		for t in text:
			t=t.strip().split()
			word=t[0]
			freq=int(t[1])
			l=phoneme_dict[word]
			for i in range(freq):
				for j in l:
					if j not in phoneme_dist:
						phoneme_dist[j]=0
					phoneme_dist[j]+=1
		for i in phoneme_dist:
			if i not in final:
				final[i]=0
			final[i]+=phoneme_dist[i]
	# print final
		f=open(dirname+'/phoneme_distribution.txt','w')
		for phoneme in sorted(phoneme_dist.keys()):
			f.write(phoneme+'\t'+str(phoneme_dist[phoneme])+'\n')

import os


artists=["BebeRexha","JenniferLopez","BritneySpears","JoniMitchell"]
path1 = "./"
for a in artists:
	path=path1+a

	for fn in os.listdir(path):
		filename = os.path.join(path, fn)
		if fn[-4:]=='.txt':
			file = open(filename, 'rt')
			text = file.read()
			dir=path+"/lyrics/"+fn[:-4]
			if not os.path.exists(dir):
				os.makedirs(dir)
			f=open(dir+"/"+fn,'w')
			f.write(text)

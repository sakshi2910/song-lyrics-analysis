import os
from nltk.stem import PorterStemmer, WordNetLemmatizer
from nltk.corpus import stopwords
from nltk.tokenize import sent_tokenize, word_tokenize
import string
from collections import defaultdict,OrderedDict

path = './Beatles/data'
vocabulary=[]
dictionary={}

file = open(path+'/beatles.txt', 'rt')
text = file.read()
file.close()
text = text.translate(None, ',')
text = text.translate(None, '"')
text = text.translate(None, '.')
text = text.translate(None, '...')
text = text.translate(None, '?')
text = text.translate(None, '!')
text = text.translate(None, ';')
text = text.translate(None, '-')
text = text.translate(None, '\'')
text = text.translate(None, '.\'')
text = text.translate(None, '(')
text = text.translate(None, ')')
text = text.translate(None, ':')
text = text.translate(None, '>')
text = text.translate(None, '<')
tokens = word_tokenize(text)
tokens = [w.lower() for w in tokens]

words = [word for word in tokens if word.isalpha()]
stop_words = set(stopwords.words('english'))
words = [w for w in words if not w in stop_words]
lemma=WordNetLemmatizer()
lemmed=[lemma.lemmatize(word) for word in words]


for w in lemmed:
	if not w in dictionary:
		dictionary[w] =1
	else:
		dictionary[w] +=1
                    
for w in lemmed:
	if w not in vocabulary:
		vocabulary.append(w)


with open(path+'/word_frequency.txt', 'w') as file:
	for key in sorted(dictionary.iterkeys()):
		file.write(str(key) +"\t\t"+ str(dictionary[key])+"\n")
file.close()
with open(path+'/unique_words.txt','w') as file:
	for key in sorted(vocabulary):
		file.write(str(key)+'\n')

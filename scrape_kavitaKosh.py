# -*- coding: utf-8 -*-
from lxml import html
import requests
import re
import codecs

def get_poem(page_content):
	page_content=page_content.split('\n')
	s=open("buffer.txt","w")
	for i in page_content:
		s.write(i.strip()+'\n')
	s.close()
	s=open('buffer.txt','r')
	page_content=s.readlines()
	poem_content=[]
	index=0
	for i in range(len(page_content)):
		if page_content[i].strip() == '<div class="poem">':
			print "found"
			index=i
			break
	print "index",index
	while page_content[index].strip()!='</div>':
		poem_content.append(page_content[index])
		index+=1
	for i in range(len(poem_content)):
		poem_content[i]=re.sub("<(\"[^\"]*\"|'[^']*'|[^'\">])*>",'',poem_content[i])
	poem_content=[i.strip() for i in poem_content if len(i)]
	return poem_content

with codecs.open("nirala_urls.txt",encoding='utf-8') as f:
    urls=f.readlines()
print urls
counter=1
for url in urls:
	page=requests.get(url.strip())
	poem=get_poem(page.content)
	s=open("./nirala/poems/poem"+str(counter),'w')
	s.write(url.encode('utf-8')+'\n\n')

	for i in range(len(poem)):
		poem[i]=re.sub("\([0-9]*\)","",poem[i])
	for i in poem:
		s.write(str(i)+'\n')
	s.close()
	counter+=1

import tensorflow as tf
import tensorflow_hub as hub
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import re
import seaborn as sns

path="./deep_learning/old_vs_new"
def load_directory_data(directory):
  data = {}
  data["songs"] = []
  data["artist"] = []
  for file_path in os.listdir(directory):
    with tf.gfile.GFile(os.path.join(directory, file_path), "r") as f:
      #print f.read()
      data["songs"].append(f.read())
      if directory[-3:] == "old":
      	data["artist"].append("old")
      elif directory[-3:] == "new":
      	data["artist"].append("new")
      print len(data["songs"])
      print "\n\n"
      print len(data["artist"])
  return pd.DataFrame.from_dict(data)

# Merge positive and negative examples, add a polarity column and shuffle.
def load_dataset(directory):
  bob_dylan_df = load_directory_data(os.path.join(directory, "old"))
  not_dylan_df = load_directory_data(os.path.join(directory, "new"))
  bob_dylan_df["polarity"] = 1
  not_dylan_df["polarity"] = 0
  return pd.concat([bob_dylan_df, not_dylan_df]).sample(frac=1).reset_index(drop=True)

# Download and process the dataset files.
#def download_and_load_datasets(force_download=False):
 # dataset = tf.keras.utils.get_file(
  #    fname="aclImdb.tar.gz", 
   #   origin="http://ai.stanford.edu/~amaas/data/sentiment/aclImdb_v1.tar.gz", 
    #  extract=True)


  #test_df = load_dataset(os.path.join(os.path.dirname(dataset), 
   #                                   "aclImdb", "test"))

  #return train_df, test_df

# Reduce logging output.
tf.logging.set_verbosity(tf.logging.ERROR)
train_df = load_dataset(path + "/train")
test_df = load_dataset(path + "/test")
train_df.head()


train_input_fn = tf.estimator.inputs.pandas_input_fn(train_df, train_df["polarity"], num_epochs=None, shuffle=True)

# Prediction on the whole training set.
predict_train_input_fn = tf.estimator.inputs.pandas_input_fn(train_df, train_df["polarity"], shuffle=False)
# Prediction on the test set.
predict_test_input_fn = tf.estimator.inputs.pandas_input_fn(test_df, test_df["polarity"], shuffle=False)

embedded_text_feature_column = hub.text_embedding_column(
    key="songs", 
    module_spec="https://tfhub.dev/google/nnlm-en-dim128/1")

estimator = tf.estimator.DNNClassifier(
    hidden_units=[500, 100],
    feature_columns=[embedded_text_feature_column],
    n_classes=2,
    optimizer=tf.train.AdagradOptimizer(learning_rate=0.003))
estimator.train(input_fn=train_input_fn, steps=1000);

train_eval_result = estimator.evaluate(input_fn=predict_train_input_fn)
test_eval_result = estimator.evaluate(input_fn=predict_test_input_fn)

print "Training set accuracy: {accuracy}".format(**train_eval_result)
print "Test set accuracy: {accuracy}".format(**test_eval_result)

def get_predictions(estimator, input_fn):
  return [x["class_ids"][0] for x in estimator.predict(input_fn=input_fn)]

LABELS = [
    "negative", "positive"
]

# Create a confusion matrix on training data.
with tf.Graph().as_default():
  cm = tf.confusion_matrix(test_df["polarity"], 
                           get_predictions(estimator, predict_test_input_fn))
  with tf.Session() as session:
    cm_out = session.run(cm)

# Normalize the confusion matrix so that each row sums to 1.
cm_out = cm_out.astype(float) / cm_out.sum(axis=1)[:, np.newaxis]

sns.heatmap(cm_out, annot=True, xticklabels=LABELS, yticklabels=LABELS);
plt.xlabel("Predicted");
plt.ylabel("True");

plt.show()
#male vs female
#Training set accuracy: 0.612450659275
#Test set accuracy: 0.600000023842

#old vs new
#Training set accuracy: 0.770282566547
#Test set accuracy: 0.779660999775






# USING WORDS ONLY
# OLD VS NEW
# Training set accuracy: 0.938866555691
# Test set accuracy: 0.732824444771

# MALE VS FEMALE
# Training set accuracy: 0.89097571373
# Test set accuracy: 0.625641047955

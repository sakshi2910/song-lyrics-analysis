import os
#change path, label, mode of f
path = '../BobMarley/lyrics'
resultpath='genderClassification.csv'

f=open(resultpath,'a')
phoneme_list=['AH','AA','IH','IY','UH','UW','AY','EY','AE','EH','AO','AW','OW','OY','K','G','D','HH','CH','JH','Y','SH','T','R','ER','TH','DH','N','NG','L','S','Z','ZH','P','F','B','M','V','W']
# for i in phoneme_list:
# 	f.write(i+',')
# f.write('Artist\n')

for fn in os.listdir(path):
	dirname = os.path.join(path, fn)
	g = open(dirname+'/'+'phoneme_distribution.txt', 'r')
	# print dirname
	l=g.readlines()
	phonemes={}
	for i in l:
		x=i.strip().split('\t')
		phonemes[x[0]]=int(x[1])
	for i in phoneme_list:
		if i in phonemes:
			f.write(str(phonemes[i])+',')
		else:
			f.write(str(0)+',')
	f.write('0\n')